#Каско ФЛ

tags: Каско ФЛ, Продукты

## Каско ФЛ штатник, Страхователь поиск, Собственник и водитель совпадает со Страхователем, Без дополнительных рисков

* Найти штатника для продукта "Каско ФЛ" филиал "28"
* Авторизация
* Переход в продукт "Каско ФЛ"
* Устнановить номер договора "1234567890" и проверить дату заключения
* Выбор продавца
* Cтрахователь поиск тестового
* Собственник совпадает со страхователем
* Водители добавить страхователя
* Заполнение транспортного средства для КАСКО
* Страховая премия в Футере должна отображаться
* Проверка и сохранение "Каско ФЛ"
* Скриншот

## Каско ФЛ штатник, Страхователь поиск, Собственник и водитель совпадает со Страхователем, Все риски

* Найти штатника для продукта "Каско ФЛ" филиал "28"
* Авторизация
* Переход в продукт "Каско ФЛ"
* Устнановить номер договора "1234567890" и проверить дату заключения
* Выбор продавца
* Cтрахователь поиск тестового
* Собственник совпадает со страхователем
* Водители добавить страхователя
* Заполнение транспортного средства для КАСКО
* Добавление риска Гражданская ответственность
* Добавление риска Несчастный случай
* Добавление риска Дополнитльное оборудование
* Добавление риска Техническая помощь
* Добавление риска ГЭП
* Страховая премия в Футере должна отображаться
* Проверка и сохранение "Каско ФЛ"
* Скриншот