# Типы оплат
=====================

tags: Сервисы, Типы оплат

     |пользователь| пароль   |
     |------------|----------|
     | в2в_автомен| gravca   |


## Типы оплат
----------------
* Получить токен graphql логин <пользователь> пароль <пароль>
* Добавить токен в заголовки запросов
* Добавить заголовок запроса Content-Type : "application/json"
* Инициализировать клиент для сервиса "STATUSMANAGER"
* POST запрос json из файла "data/services/typePay.json", путь "typepayservice/typepaylist"
* Проверить ответ на соответствие jsonSchema "data/json-schemas/typePaySchema.json"