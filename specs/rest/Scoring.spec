# Скоринг
=====================

tags: Сервисы, Скоринг

     |пользователь| пароль   |
     |------------|----------|
     | в2в_автомен| gravca   |


## Скоринг
----------------
* Добавить заголовок запроса "Authorization" : "REST ZGlhc29mdDpkaWFzb2Z0MTIz"
* Добавить заголовок запроса Content-Type : "application/json"
* Инициализировать клиент для сервиса "SCORING"
* POST запрос json из файла "data/services/scoring.json", путь "scoring"
* Проверить ответ на соответствие jsonSchema "data/json-schemas/scoringSchema.json"