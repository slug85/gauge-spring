# СОА POST JSON
=====================

tags: Сервисы, СОА

     |пользователь| пароль   |                   метод        |                   путь_json_схемы             |
     |------------|----------|--------------------------------|-----------------------------------------------|
     | в2в_автомен| gravca   | docflowfromcontractform        | data/json-schemas/docflowfromcontractResponse.json |

## SoaIntegration POST JSON
----------------
* Получить токен graphql логин <пользователь> пароль <пароль>
* Добавить токен в заголовки запросов
* Добавить заголовок запроса Content-Type : "application/json"
* Инициализировать клиент для сервиса "soaRest"
* POST запрос json из файла "data/services/docflowfromcontractBody.json", путь <метод>
* Проверить ответ на соответствие jsonSchema <путь_json_схемы>
