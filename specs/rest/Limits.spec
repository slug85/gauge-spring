# Лимиты
=====================

tags: Сервисы, Лимиты

     |пользователь| пароль   |
     |------------|----------|
     | в2в_автомен| gravca   |


## Лимиты
----------------
* Получить токен graphql логин <пользователь> пароль <пароль>
* Добавить токен в заголовки запросов
* Добавить заголовок запроса Content-Type : "application/json"
* Инициализировать клиент для сервиса "STATUSMANAGER"
* POST запрос json из файла "data/services/limits.json", путь "limitservice/limit"
* Проверить ответ на соответствие jsonSchema "data/json-schemas/limitsSchema.json"