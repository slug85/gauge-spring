# TRANSPORT VERIFIER
=====================

tags: Сервисы, СОА

     |пользователь| пароль   |
     |------------|----------|
     | в2в_автомен| gravca   |

## TransportVerifier
----------------
* Получить токен graphql логин <пользователь> пароль <пароль>
* Добавить токен в заголовки запросов
* Добавить заголовок запроса Content-Type : "application/json"
* Инициализировать клиент для сервиса "securityCheck"
* POST запрос json из файла "data/services/createCheckSb.json", путь "createchecksb"
* Проверить ответ на соответствие jsonSchema "data/json-schemas/createCheckSbSchema.json"
* Сохранить значение из JSON "newObj"
* Проверка созданной заявки СБ
* Проверить ответ на соответствие jsonSchema "data/json-schemas/eventCheckSbSchema.json"
* Проверить значение поля в JSON через JsonPath "isError" "0"
* Проверка полей заявки
* Проверить ответ на соответствие jsonSchema "data/json-schemas/fieldsSchema.json"