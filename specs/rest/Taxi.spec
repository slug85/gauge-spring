# Такси
=====================

tags: Сервисы, Такси

     |пользователь| пароль   |
     |------------|----------|
     | в2в_автомен| gravca   |


## Такси
----------------
* Получить токен graphql логин <пользователь> пароль <пароль>
* Добавить токен в заголовки запросов
* Добавить заголовок запроса Content-Type : "application/json"
* Инициализировать клиент для сервиса "STATUSMANAGER"
* POST запрос json из файла "data/services/taxi.json", путь "taxirestservice/taxi"
* Проверить ответ на соответствие jsonSchema "data/json-schemas/taxiSchema.json"