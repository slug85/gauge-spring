# Черный ящик
=====================

tags: Сервисы, Черный ящик

     |пользователь| пароль   |
     |------------|----------|
     | в2в_автомен| gravca   |

     
## Черный ящик
----------------
* Получить токен graphql логин <пользователь> пароль <пароль>
* Добавить токен в заголовки запросов
* Добавить заголовок запроса Content-Type : "application/json"
* Инициализировать клиент для сервиса "BLACKBOXSERVICE"
* POST запрос json из файла "data/services/blackbox.json", путь "doCheckBlackBox"
* Проверить ответ на соответствие jsonSchema "data/json-schemas/blackBoxSchema.json"