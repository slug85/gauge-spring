# СОА GET XML
=====================

tags: Сервисы, СОА

     |пользователь| пароль   |                   метод                  |                   путь_xsd             |
     |------------|----------|------------------------------------------|----------------------------------------|
     | в2в_автомен| gravca   | consistentlistvalues?contractId=99171566 | data/xsd-schemas/consistentlistvalues.xsd  |
     | в2в_автомен| gravca   | requiredrequestfields?productId=102066   | data/xsd-schemas/requiredrequestfields.xsd |

## SoaIntegration
----------------
* Получить токен graphql логин <пользователь> пароль <пароль>
* Добавить токен в заголовки запросов
* Добавить заголовок запроса Content-Type : "application/json"
* Инициализировать клиент для сервиса "soaRest"
* GET запрос путь <метод>
* Проверить ответ на соответствие xmlSchema <путь_xsd>
* Проверить значение поля в XML "Продукт" "102066"