package root.db;

import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Component;

@Component
public class ConnectionBeans {

    @Bean
    public DriverManagerDataSource createDataSource(){

        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("oracle.jdbc.OracleDriver");
        dataSource.setPassword(System.getenv("DB.PASSWORD"));
        dataSource.setUrl(System.getenv("DB.URL"));
        dataSource.setUsername(System.getenv("DB.USER"));
        return dataSource;
    }


}
