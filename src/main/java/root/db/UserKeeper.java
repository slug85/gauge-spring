package root.db;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;
import root.entities.User;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Repository
public class UserKeeper {

    private List<User> users = Collections.synchronizedList(new ArrayList<>());
    private Log LOGGER = LogFactory.getLog(this.getClass());

    public void remove(User user) {
        if (user != null) {
            if (users.contains(user)) {
                LOGGER.info("УДАЛИТЬ ПОЛЬЗОВАТЕЛЯ " + user.getBrief());
                users.remove(user);
            }
        }
    }

    synchronized Set<String> getUserIdsList() {
        return users
                .stream()
                .filter(c -> c.getUserId() != null)
                .map(User::getUserId)
                .collect(Collectors.toSet());
    }


    public void add(User user) {
        if(user != null) {
            users.add(user);
        }
    }

}
