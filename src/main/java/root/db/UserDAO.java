package root.db;

import root.entities.User;

public interface UserDAO {

    User getUser(String productId, String orgstructureId);
    void updatePassword(User user);

}
