package root.db;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;

import static junit.framework.TestCase.fail;

@Repository
public class DBAppUrlDAOImpl implements AppUrlDAO {

    private NamedParameterJdbcTemplate jdbcTemplate;

    public DBAppUrlDAOImpl(DataSource dataSource) {
        JdbcTemplate template = new JdbcTemplate(dataSource);
        template.setQueryTimeout(400);
        jdbcTemplate = new NamedParameterJdbcTemplate(template);
    }

    @Override
    public String getAppUrl() {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("brief", "ANGULAR_ROOT_URL");
        String url;
        try {
            url = jdbcTemplate.queryForObject("SELECT  b.val2 FROM b2b_parameters b WHERE b.brief = :brief", params, String.class);
        } catch (DataAccessException ex) {
            fail("невозможно счтать APP_URL из базы");
            return null;
        }
        return url;
    }
}
