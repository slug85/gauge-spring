package root.db;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import root.entities.Seller;
import root.entities.SellerType;
import root.entities.User;
import root.gauge.LoginSteps;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.util.Set;

import static java.sql.Types.NUMERIC;
import static junit.framework.TestCase.fail;

@SuppressWarnings("SqlResolve")
@Repository
public class EmployerDAOImpl implements UserDAO, InitializingBean {

    private final DataSource dataSource;
    private NamedParameterJdbcTemplate jdbcTemplate;
    private static final Log logger = LogFactory.getLog(LoginSteps.class);

    @Autowired
    public EmployerDAOImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Autowired
    UserKeeper userKeeper;

    @Override
    public synchronized User getUser(String productId, String orgstructureId) {
        MapSqlParameterSource params = new MapSqlParameterSource();

        Set<String> userIdList = userKeeper.getUserIdsList();
        String inUsersCriteria = "";
        if (userIdList.size() != 0) {
            params.addValue("userIdList", userIdList);
            inUsersCriteria = " AND USERID NOT IN (:userIdList) ";
        }

        params.addValue("productId", productId, NUMERIC);
        params.addValue("docKindId", 143, NUMERIC);
        params.addValue("orgstructureId", orgstructureId, NUMERIC); // для еосаго 730

        return jdbcTemplate.queryForObject("SELECT *\n" +
                "  FROM (SELECT s.sellerid,\n" +
                "               s.sellertypeid,\n" +
                "               a.userid,\n" +
                "               s.endivision_orgstructureid,\n" +
                "               s.entityid,\n" +
                "               NULL as BSO,\n" +
                "               (select restlimit\n" +
                "                  from table(mathunits.fGetLimitOSAGO(aDateCalc => trunc(sysdate),\n" +
                "                                                      aСodeIKP => v.SellerCode,\n" +
                "                                                      aRegion   => :orgstructureId,\n" +
                "                                                      aTypeDoc  => :docKindId))) AS osago_limit,\n" +
                "               (select count(1)\n" +
                "                  from contractins c\n" +
                "                 where c.authorid = a.userid\n" +
                "                   and c.seller1id = s.sellerid\n" +
                "                   and c.signdate > sysdate - 90\n" +
                "                   and c.insproductid = :productId) polcnt,\n" +
                "               a.name,\n" +
                "               v.SELLERNAME,\n" +
                "               a.agent_phonemobile,\n" +
                "               v.sellercode,\n" +
                "               a.brief,\n" +
                "               a.email_address,\n" +
                "               a.status,\n" +
                "               null as BSOA7SER\n" +
                "          FROM osagosellers s\n" +
                "          JOIN v_b2b_seller v\n" +
                "            ON v.sellerid = s.sellerid\n" +
                "          JOIN b2b_rel_links brl\n" +
                "            ON v.SellerID = brl.id2\n" +
                "          JOIN applicationuser a\n" +
                "            ON a.userid = brl.id1\n" +
                "          JOIN osagosellerstype ost\n" +
                "            on v.sellertypeid = ost.sellertypeid\n" +
                "          join employee e\n" +
                "            on e.userid = a.userid\n" +
                "         WHERE s.sellertypeid = 1\n" +
                "           AND s.endivision_orgstructureid = :orgstructureId\n" +
                "           AND brl.reltypeid = 7)\n" +
                " WHERE status NOT IN (200, 301, 666)\n" +
                "   AND userId NOT IN ('10',\n" +
                "                      '3159',\n" +
                "                      '60',\n" +
                "                      '60',\n" +
                "                      '30565',\n" +
                "                      '80003',\n" +
                "                      '60765',\n" +
                "                      '60766',\n" +
                "                      '60767',\n" +
                "                      '60768',\n" +
                "                      '60764',\n" +
                "                      '181',\n" +
                "                      '141057',\n" +
                "                      '1851')\n" +
                 inUsersCriteria +
                "   AND ROWNUM = 1\n" +
                "   AND email_address IS NOT NULL\n" +
                "   AND osago_limit > 0\n" +
                " ORDER BY osago_limit desc\n", params, (ResultSet rs, int rowNum) -> {

            User a = new User();
            Seller s = new Seller();
            a.setBrief(rs.getString("brief"));
            a.setUserId(rs.getString("userid"));
            s.setName(rs.getString("SELLERNAME"));
            s.setSellerCode(rs.getString("SELLERCODE"));
            s.setSellerId(rs.getString("SELLERID"));
            s.setSellerType(SellerType.findById(rs.getString("SELLERTYPEID")));
            a.setSeller(s);
            return a;
        });
    }

    @Override
    public synchronized void updatePassword(User user) {

        MapSqlParameterSource params = new MapSqlParameterSource();
        if (user.getUserId() == null) {
            fail("отсутствует id пользователя");
        }
        params.addValue("userId", user.getUserId());
        String query =
                " update applicationuser au set AU.PASSWORD=b2b_auto.StrMD5ToHex(to_char(au.userid) || '1') " +
                        "where AU.USERID = :userId" +
                        " and au.brief not like '%втотест%'" +
                        " and au.brief not like '%втомен%'";

        try {
            jdbcTemplate.update(
                    query,
                    params
            );
            user.setPassword("1");
        } catch (DataAccessException ex) {
            logger.error(ex.getMessage());
            fail("НЕВОЗМОЖНО СДЕЛАЙТЬ АПДЕЙТ ПАРОЛЬ ЮЗЕРУ: " + ex.getMessage());
        }

    }

    @Override
    public void afterPropertiesSet() throws Exception {
        JdbcTemplate template = new JdbcTemplate(dataSource); //максимальное время выполнения запросов можно задать только в JdbcTemplate
        template.setQueryTimeout(400); //сек
        jdbcTemplate = new NamedParameterJdbcTemplate(template);
    }


}
