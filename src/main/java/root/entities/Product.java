package root.entities;

import java.util.Arrays;
import java.util.function.Predicate;

/**
 * Created by sergey.lugovskoi on 07.06.2017.
 */
public enum Product {
    KS_ALL(new Builder("Комплексное страхование (все виды)", 0, false, 92, "КомплексноеСтрахованиеВсеВиды", false)),
    OSAGO(new Builder("ОСАГО", 0, 86, "ОСГО", false, false, true)),
    OSAGO_V2(new Builder("ОСАГО v2", 0, 86, "ОСГО", false, true, true)),
    KASKOFL(new Builder("Каско ФЛ", 0, 93, "ТФ", false, false, true).setKasko(true)),
    KASKOFL_V2(new Builder("Каско ФЛ v2", 0, 93, "ТФ", false, true, true).setKasko(true)),
    KASKOUL(new Builder("Каско ЮЛ", 0, 94, "ТЮ", false, false, true).setKasko(true)),
    KASKOUL_V2(new Builder("Каско ЮЛ v2", 0, 94, "ТЮ", false, true, true).setKasko(true)),
    KLESCH(new Builder("Антиклещ ФЛ", 1, true, 30374, "М98", false)),
    AKLESCH_V2(new Builder("Антиклещ ФЛ v2", 1, true, 30374, "М98", false, true)),
    VACATIONTIME(new Builder("На время отпуска v2", 1, true, 120786, "М98", false, true)),
    SCHOOLTIME(new Builder("Школьная пора", 1, true, 70945, "М98", false, true)),
    MULTISPORTSMAN(new Builder("Мультиспортсмен", 1, true, 72589, "НСГФМ", false)),
    MULTISPORTSMAN_V2(new Builder("Мультиспортсмен v2", 1, true, 72589, "НСГФМ", false, true)),
    MULTISPORTSMANPLUS_V2(new Builder("Мультиспортсмен + v2", 1, true, 97416, "НСГФМ", false, true)),
    EXPRESSFLAT(new Builder("Квартира Экспресс", 0, true,  30355, "ИФКЭ", false)),
    EXPRESSHOUSE(new Builder("Дом Экспресс", 0, false, 30282, "ИФДЭ", false)),
    EXPRESSHOUSE_V2(new Builder("Дом Экспресс v2", 0, false, 30282, "ИФДЭ", false, true)),
    BUSINESSDESIGNER(new Builder("Бизнес-Конструктор", 1, false, 59277, "ИМЮГО", false)),
    BUSINESSDESIGNER_V2(new Builder("Бизнес-Конструктор v2", 0, false, 59277, "ИМЮГО", false, true)),
    GREENCARD(new Builder("Зеленая карта", 0, 278, "ЗК", false, false, true)),
    GREENCARD_V2(new Builder("Зеленая карта v2", 0, 278, "ЗК", false, true, true)),
    SAFESTYLE(new Builder("Стиль безопасности", 1, false, 59123, "НСБФСБ", false)),
    SAFESTYLE_V2(new Builder("Стиль безопасности v2", 1, false, 59123, "НСБФСБ", false, true)),
    SAFESTYLEPLUS(new Builder("Стиль безопасности +", 1, false, 73905, "НСБФСБФ", false)),
    PROTECTIONEXPRESS(new Builder("Экспресс-Защита", 0, true, 71017, "НСГФЭ", false)),
    PROTECTIONEXPRESS_V2(new Builder("Экспресс-Защита v2", 0, true, 71017, "НСГФЭ", false, true)),
    GREENLIGHT(new Builder("Зеленый свет+", 0, true, 73784, "НСГФЗ", false)),
    GREENLIGHT_V2(new Builder("Зеленый свет+ v2", 0, true, 73784, "НСГФЗ", false, true)),
    KASKOTTS(new Builder("КАСКО ТТС (Казань)", 0, 62521, "ТТС", false, false, true).setKasko(true)),
    KASKO10050(new Builder("100 за 50", 0, 298, "ТФП50", false, false, true).setKasko(true)),
    KASKO10050_V2(new Builder("100 за 50 v2", 0, 298, "ТФП50", false, true, true).setKasko(true)),
    KASKOMASTER(new Builder("Каско мастер", 0, 70851, "ТФМАС", false, false, true).setKasko(true)),
    KASKOINSTALLMENT(new Builder("#каско_рассрочка", 1, 87369, "ТФМ", false, false, true).setKasko(true)),
    KASKOINSTALLMENT_V2(new Builder("#каско_рассрочка v2", 1, 87369, "ТФМ", false, true, true).setKasko(true)),
    KASKOAGAIN(new Builder("КАСКоснова", 0, 124103, "ТФУТ", false, false, true).setKasko(true)),
    KASKOAGAIN_V2(new Builder("КАСКоснова v2", 0, 124103, "ТФУТ", false, true, true).setKasko(true)),
    GEP(new Builder("ГЭП", 1, 30233, "ГЭП", false, false, true)),
    VAZULTRA(new Builder("ВАЗ-Ультра", 0, 100140, "ТФВАЗУ", false, false, true).setKasko(true)),
    VAZULTRA_V2(new Builder("ВАЗ-Ультра v2", 0, 100140, "ТФВАЗУ", false, true, true).setKasko(true)),
    VAZLIGHT(new Builder("ВАЗ-Лайт", 0, 100141, "ТФВАЗЛ", false, false, true).setKasko(true)),
    VAZLIGHT_V2(new Builder("ВАЗ-Лайт v2", 0, 100141, "ТФВАЗЛ", false, true, true).setKasko(true)),
    VAZPROFI(new Builder("ВАЗ-Профи", 0, 100144, "ТФВАЗП", false, false, true).setKasko(true)),
    VAZPROFI_V2(new Builder("ВАЗ-Профи v2", 0, 100144, "ТФВАЗП", false, true, true).setKasko(true)),
    MAZDAFL(new Builder("Mazda страхование ФЛ", 0, 56972, "ТФМС", false, false, true).setKasko(true)),
    MAZDAFL_V2(new Builder("Mazda страхование ФЛ v2", 0, 56972, "ТФМС", false, true, true).setKasko(true)),
    KASKO_OFFICIAL_PLUS_V2(new Builder("КАСКОфициал+ v2", 0, 143223, "ТФКП", false, true, true).setKasko(true)),
    MYFLAT(new Builder("Моя квартира", 1, false, 109456, "ИФМК2", false, true).setHasInsObject(true)),
    MYHOUSE(new Builder("Мой дом", 1, false, 112356, "ИФМД2", false, true).setHasInsObject(true)),

    VZR(new Builder("ВЗР", 1, true, 116501, "МПВЗР", false, true)
            .setOnlyFacsimile(true)),

    MULTI(new Builder("МультиПродукт v2", 1, true, 134872, "МПО", false, true)
            .setOnlyFacsimile(true)),

    TELEMEDICINE(new Builder("Телемедицина v2", 1, true, 137924, "М28", false, true)
            .setOnlyFacsimile(true)),

    MORTGAGE(new Builder()
            .setName("Комплексное ипотечное страхование (КИС) v2")
            .setNumMethod(1)
            .setProductId(130682)
            .setAngular(true)
            .setSerieName("ИС")
            .setRider(false).setHasInsObject(true)),

    //Райдеры мультипродукт
    MULTICARD(new Builder("МультиКарта", 1, false, 133640, "МПДБКМ", true, true)),
    MULTIOPORA(new Builder("МультиОпора", 1, false, 133642, "МПЗПРМ", true, true)),
    MULTIJURIST(new Builder("МультиЮрист", 1, false, 133646, "МПФРЮП", true, true)),
    MULTISOBITIE(new Builder("МультиСобытие", 1, false, 133648, "МПФРНМ", true, true)),
    MULTIKVARTIRA(new Builder("МультиКвартира", 1, false, 133656, "МПФРНМ", true, true)),
    MULTIDOCTOR(new Builder("МультиДоктор", 1, false, 133658, "МПДМС", true, true)),
    MULTISLUCHAI(new Builder("МультиСлучай", 1, false, 133659, "НСБЮфМП", true, true)),
    MULTIOTDIH(new Builder("МультиОтдых", 1, false, 133661, "МПВЗР", true, true)),
    MULTIDOM(new Builder("МультиДом", 1, false, 134243, "ИФМПД", true, true)),
    MULTITEHNIKA(new Builder("МультиТехника", 1, false, 134291, "ИФМПГ", true, true)),

    //Райдеры
    AUTOSTANDART(new Builder("Автостандарт", 1, false, 76444, "ГОНС", true)),
    KASKOZASHITA(new Builder("КАСКОзащита", 1, false, 76444, "ГОНС", true)),
    ZASHITA(new Builder("ЗАЩИТА", 1, false, 98689, "ТФЗ", true)),
    EXPRESS_FLAT(new Builder("Квартира Экспресс", 0, false, 30355, "ИФКЭ", true)),
    EXPRESS_FLAT_V2(new Builder("Квартира Экспресс v2", 0, false, 30355, "ИФКЭ", true, true)),
    EXPRESSGO(new Builder("Квартира Экспресс ГО", 1, false, 76048, "ИФКЭГО", true)),

    // СОА продукты
    IMUJURL(new Builder("Имущество юр. лиц (поименованные риски) (ИМЮ)", 0, false, 102066, "ИМЮ", false)),
    SPECT(new Builder("Спецтехника (ССТ)", 0, false, 93347, "ССТ", false)),
    CARGO(new Builder("Грузы (разовые перевозки) (СГ)", 0, false, 94260, "СГ", false));


    private Builder builder;

    Product(Builder builder) {
        this.builder = builder;
    }

    public String getName() {
        return builder.getName();
    }

    public boolean isAngular() {
        return builder.isAngular();
    }

    public int getProductId() {
        return builder.getProductId();
    }

    public boolean isRider() {
        return builder.isRider();
    }

    public boolean isNeedTS() {
        return builder.isNeedTS();
    }

    public boolean isKasko() { return builder.isKasko(); }

    public boolean hasFacsimile() {
        return builder.isHasFacsimile();
    }

    public boolean isFascimileOnly() {
        return builder.isFascimileOnly();
    }

    public boolean hasInsObject(){
        return builder.hasInsObject;
    }


    /*
     * 1 - генерация выделенных номеров
     * 0 - ручной ввод
     * 2 - хз, но такие есть в b2b_insproduct
     *
     */
    public int getNumMethod() {
        return builder.getNumMethod();
    }

    public static Product findById(int id) {
        for (Product p : values()) {
            if (p.getProductId() == id) {
                return p;
            }
        }
        return null;
    }

    @SuppressWarnings("OptionalGetWithoutIsPresent")
    public static Product find(Predicate<Product> predicate){
        return Arrays.stream(Product.values()).filter(predicate).findAny().get();
    }

    private static class Builder{

        private String name;
        private String serieName;
        private int productId;
        private boolean isRider;
        private int numMethod;          //БСО не вдается а номер генерится процедурой выделенных номеров? 1 - генерация 0 - ввод
        private boolean isAngular;
        private boolean hasFacsimile;
        private boolean onlyFacsimile = false;
        private boolean needTS; //для оформления продукта требуется ТС?
        private boolean hasInsObject = false; //Добавляются ли объекты страхования (если true, то считывает из файла)
        private boolean isKasko = false; //Каско продукт или нет

        Builder() {
        }

        public String getName() {
            return name;
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public String getSerieName() {
            return serieName;
        }

        public Builder setSerieName(String serieName) {
            this.serieName = serieName;
            return this;
        }

        int getProductId() {
            return productId;
        }

        Builder setProductId(int productId) {
            this.productId = productId;
            return this;
        }

        public boolean isKasko() {
            return isKasko;
        }

        public Builder setKasko(boolean kasko) {
            isKasko = kasko;
            return this;
        }

        public Builder setHasInsObject(boolean hasInsObject) {
            this.hasInsObject = hasInsObject;
            return this;
        }

        boolean isRider() {
            return isRider;
        }

        Builder setRider(boolean rider) {
            isRider = rider;
            return this;
        }

        int getNumMethod() {
            return numMethod;
        }

        public Builder setNumMethod(int numMethod) {
            this.numMethod = numMethod;
            return this;
        }

        public boolean isAngular() {
            return isAngular;
        }

        Builder setAngular(boolean angular) {
            isAngular = angular;
            return this;
        }

        boolean isHasFacsimile() {
            return hasFacsimile;
        }

        public Builder setHasFacsimile(boolean hasFacsimile) {
            this.hasFacsimile = hasFacsimile;
            return this;
        }

        boolean isFascimileOnly() {
            return onlyFacsimile;
        }

        Builder setOnlyFacsimile(@SuppressWarnings("SameParameterValue") boolean onlyFacsimile) {
            this.onlyFacsimile = onlyFacsimile;
            return this;
        }

        boolean isNeedTS() {
            return needTS;
        }

        public Builder setNeedTS(boolean needTS) {
            this.needTS = needTS;
            return this;
        }

        Builder(String name, int numMethod, boolean hasFacsimile, int productId, String serieName, boolean isRider) {
            this.name = name;
            this.numMethod = numMethod;
            this.hasFacsimile = hasFacsimile;
            this.serieName = serieName;
            this.isRider = isRider;
            this.productId = productId;
            this.isAngular = false;
            this.needTS = false;
        }

        Builder(String name, int numMethod, boolean hasFacsimile, int productId, String serieName, boolean isRider, boolean isAngular) {
            this.name = name;
            this.numMethod = numMethod;
            this.hasFacsimile = hasFacsimile;
            this.serieName = serieName;
            this.isRider = isRider;
            this.productId = productId;
            this.isAngular = isAngular;
            this.needTS = false;
        }

        Builder(String name, int numMethod, int productId, String serieName, boolean isRider, boolean isAngular, boolean needTS) {
            this.name = name;
            this.numMethod = numMethod;
            this.hasFacsimile = false;
            this.serieName = serieName;
            this.isRider = isRider;
            this.productId = productId;
            this.isAngular = isAngular;
            this.needTS = needTS;
        }
    }
}
