package root.entities;

/**
 * Created by sergey.lugovskoi on 23.08.2017.
 */
public class User {

    private String userId;
    private String orgStructureId;
    private String brief;
    private String name;
    private String password = "gravca";
    private Seller seller;
    private String token = "";

    public String getUserId() {
        return userId;
    }

    public User setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public String getOrgStructureId() {
        return orgStructureId;
    }

    public User setOrgStructureId(String orgStructureId) {
        this.orgStructureId = orgStructureId;
        return this;
    }

    public String getBrief() {
        return brief;
    }

    public String getToken() {
        return token;
    }

    public User setToken(String token) {
        this.token = token;
        return this;
    }

    public User setBrief(String brief) {
        this.brief = brief;
        return this;
    }

    public String getName() {
        return name;
    }

    public User setName(String name) {
        this.name = name;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public User setPassword(String password) {
        this.password = password;
        return this;
    }

    public Seller getSeller() {
        return seller;
    }

    public User setSeller(Seller seller) {
        this.seller = seller;
        return this;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId='" + userId + '\'' +
                ", orgStructureId='" + orgStructureId + '\'' +
                ", brief='" + brief + '\'' +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
