package root.entities;

/**
 * Created by sergey.lugovskoi on 20.12.2017.
 */
public enum SellerType {

    SHTAT("1", true),
    AGENT_FL("2", false),
    AGENT_UL("3", false),
    BROKER("4", false),
    BANK("5", false)
    ;


    SellerType(String id, boolean isEmployer) {
        this.id = id;
        this.isEmployer = isEmployer;
    }

    private String id;
    private boolean isEmployer;


    public boolean isEmployer() {
        return isEmployer;
    }

    public static SellerType findById(String id){
        for(SellerType p : values()){
            if(p.getId().equals(id)){
                return p;
            }
        }
        return null;
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return String.valueOf(id);
    }
}
