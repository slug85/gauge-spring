package root.entities;

import java.util.Objects;

public class Seller {

    private SellerType sellerType;
    String sellerId;
    String sellerCode;
    String name;

    public SellerType getSellerType() {
        return sellerType;
    }

    public Seller setSellerType(SellerType sellerType) {
        this.sellerType = sellerType;
        return this;
    }

    public String getSellerId() {
        return sellerId;
    }

    public Seller setSellerId(String sellerId) {
        this.sellerId = sellerId;
        return this;
    }

    public String getSellerCode() {
        return sellerCode;
    }

    public Seller setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
        return this;
    }

    public String getName() {
        return name;
    }

    public Seller setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Seller seller = (Seller) o;
        return getSellerType() == seller.getSellerType() &&
                getSellerId().equals(seller.getSellerId()) &&
                getSellerCode().equals(seller.getSellerCode()) &&
                Objects.equals(getName(), seller.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSellerType(), getSellerId(), getSellerCode(), getName());
    }
}
