package root.setup;

import org.junit.Assert;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;

@Component
public class FileWorker {


    private File readFile(Resource resource) {
        try {
            return resource.getFile();
        } catch (IOException e) {
            Assert.fail(e.getMessage());
            return null;
        }
    }

    public String read(String fileName) {
        Resource resource = new ClassPathResource(fileName);
        File file = readFile(resource);
        String s;
        try {
            assert file != null;
            s = new String(java.nio.file.Files.readAllBytes(file.toPath()));
            return s;
        } catch (IOException e) {
            Assert.fail();
            return null;
        }
    }


}
