package root.gauge;

import com.thoughtworks.gauge.Step;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import root.entities.Seller;
import root.entities.User;
import root.ui.ScenarioStore;

@Component
public class IKPSteps extends CommonSteps{

    @Autowired
    private ScenarioStore scenarioStore;

    @Step("Продавец 1")
    public void sellerOne() {
        Seller seller = scenarioStore.getData(DataKeys.User, User.class).getSeller();
        scenarioStore.currentContext().set("Продавец", seller.getSellerCode());
        loading();
    }


    @Step("Куратор")
    public void curator() {

    }
}
