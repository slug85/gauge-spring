package root.gauge;

public enum DataKeys {
    Response,
    InsurerId,
    AddressId,
    PassportId,
    VUId,
    Mobile,
    Request,
    Product,
    User,
    CarPrice,
    TempValue;
}
