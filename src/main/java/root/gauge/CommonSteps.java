package root.gauge;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.ex.ElementNotFound;
import com.thoughtworks.gauge.Gauge;
import com.thoughtworks.gauge.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import root.db.AppUrlDAO;
import root.ui.Context;
import root.ui.ScenarioStore;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static root.gauge.DateUtils.*;
import static root.ui.Context.context;

@Component
public class CommonSteps {

    @Autowired
    private ScenarioStore scenarioStore;

    @Autowired
    @Qualifier("DBAppUrlDAOImpl")
    private AppUrlDAO appUrlDAO;

    @Step("Открыть фрейм")
    public void open() {
        String appUrl = System.getenv("APP_URL");

        if(appUrl == null) {
            //если не задана APP_URL в окружении то берем из базы
            appUrl = appUrlDAO.getAppUrl();
            Gauge.writeMessage("APP_URL взят из базы %s", appUrl);
        }else{
            Gauge.writeMessage("APP_URL взят из env %s", appUrl);
        }

        Selenide.open(appUrl);
    }

    @Step("Открыть продукт <productName>")
    public void openProduct(String productName) {

        $(byText(productName)).click();

    }

    @Step({"Панель <elementId>", "Диалог <elementId>", "Появляется Диалог <elementId>", "Риск <elementId>", "Контекст <elementId>", "Раздел <elementId>"})
    public void setContextToDataStore(String elementId) {
        Context panel = context(elementId);
        scenarioStore.currentContext(panel);
    }

    @Step("<elementId> = <value>")
    public void setElementValue(String elementId, String value) {
        Context context = scenarioStore.currentContext();
        // если это type ahead select, то еще ждем, пока появится значение, которое мы поставили в input и потом кликаем на него.
        if ("typeahead-select".equals(context.elementType(elementId))) {
            context.setSlowyly(elementId, value, 300);
            context.pressEnter(elementId);
        } else if (context.checkbox(elementId)) {
            boolean boolValue = "ДА".equalsIgnoreCase(value);
            context.set(elementId, boolValue);
        } else {
            context.set(elementId, value);
        }
    }

    @Step("Заполнить страну <Страна>")
    public void country(String countryName) {
        SelenideElement country = $(By.xpath("//*[@*='Введите страну']//input"));
        country.click();
        country.sendKeys(countryName);
        clickAutocomplete(countryName.toUpperCase());
        country.pressEnter();

    }

    private void clickAutocomplete(String text) {

        SelenideElement firstVisible = $(By.xpath("//div[@class='ng2-menu-item' and descendant::*[contains(text(),'" + text + "')]]"));
        firstVisible.waitUntil(visible, 15000);
        firstVisible.click();
        firstVisible.waitUntil(Condition.hidden, 5000);
    }

    @Step({"<elementId> должно содержать <text>", "<elementId> должен содержать <text>", "<elementId> должна содержать <text>"})
    public void shouldHave(String elementId, String text) {
        scenarioStore.currentContext().check(elementId, text);
    }

    @Step({"<elementId> должно быть <text>", "<elementId> должна быть <text>", "<elementId> должен быть <text>"})
    public void mustBe(String elementId, String text) {
        scenarioStore.currentContext().check(elementId, text);
    }

    @Step("Дождаться элемента <elementId>")
    public void waitForElement(String elementId) {
        scenarioStore.currentContext().waitElem(elementId);
    }

    @Step("Дождаться текста <text>")
    public void waitForText(String text) {
        scenarioStore.currentContext().waitText(text);
    }

    @Step("Кликнуть <elementId>")
    public void click(String elementId) {
        scenarioStore.currentContext().click(elementId);
    }

    @Step("Нажать Tab на <elementId>")
    public void pressTab(String elementId) {
        scenarioStore.currentContext().pressTab(elementId);
    }

    @Step("Кликнуть текст <elementId>")
    public void clickText(String elementId) {
        scenarioStore.currentContext().clickText(elementId);
    }

    @Step("<elementId> должна быть Сегодня")
    public void setDateToToday(String elementId) {
        scenarioStore.currentContext().check(elementId, today());
    }

    @Step("<elementId> = Год назад")
    public void setYearToYearAgo(String elementId) {
        scenarioStore.currentContext().set(elementId, todayMinusYear());
    }

    @Step("<elementId> = Дата год назад")
    public void setDateToYearAgo(String elementId) {
        scenarioStore.currentContext().set(elementId, todayMinusYearDate());
    }


    @Step("<elementId> = Сегодня")
    public void setDateToday(String elementId) {
        scenarioStore.currentContext().set(elementId, today());
    }

    @Step("<elementId> = Сегодня-плюс-месяц")
    public void setDatePlusMonth(String elementId) { scenarioStore.currentContext().set(elementId, todayPlusOneMonth()); }

    @Step("<elementId> = Через Год")
    public void setDateFuture(String elementId) {
        scenarioStore.currentContext().set(elementId, todayPlusYearDate());
    }

    @Step("<elementId> = Сегодня-сейчас")
    public void setTodayAndNow(String elementId) {
        scenarioStore.currentContext().set(elementId, todayAndNow());
    }

    @Step("<elementId> = Прошлый год")
    public void setPrevYear(String elementId) {
        scenarioStore.currentContext().set(elementId, prevYear());
    }

    @Step("<elementId> должна быть Вчера-плюс-год")
    public void setYesterdayPlusYear(String elementId) {
        scenarioStore.currentContext().check(elementId, yesterdayPlusYear());
    }

    @Step({"Установить галку <elementId>"})
    public void setCheckbox(String elementId) {
        scenarioStore.currentContext().set(elementId, true);
    }

    @Step("Убрать галку <elementId>")
    public void removeCheckbox(String elementId) {
        scenarioStore.currentContext().set(elementId, false);
    }

    @Step("Нажать Enter на <elementId>")
    public void pressEnterOnElement(String elementId) {
        scenarioStore.currentContext().pressEnter(elementId);
    }

    @Step("Не должно быть элемента <elementId>")
    public void shouldNotExist(String elementId) {
        scenarioStore.currentContext().checkNotExist(elementId);
    }

    @Step({"Тест-кейс не доделан", "НЕ СДЕЛАНО"})
    public void NotImplemented() {
        throw new UnsupportedOperationException("Текущий тест-кейс не доделан.");
    }

    @Step({"<elementId> заполнен", "<elementId> заполнена", "<elementId> заполнено"})
    public void checkNotEmpty(String elementId) {
        scenarioStore.currentContext().checkNotEmpty(elementId);
    }

    @Step("Дождаться загрузки")
    public void loading() {
        SelenideElement spinner = $(By.xpath("//i[contains(@class,'fa-spinner')]"));
        SelenideElement progressBar = $(By.xpath("//div[contains(@class,'active') and contains(@class,'progress-bar')]"));
        SelenideElement loading = $(By.xpath("//div[contains(@class,'loading-icon')]"));
        try {
            if (progressBar.is(visible)) {
                progressBar.waitUntil(Condition.hidden, 120000);
            }
            if (spinner.is(visible)) {
                spinner.waitUntil(Condition.hidden, 120000);
            }
        } catch (StaleElementReferenceException | ElementNotFound ex) {
        }
        try {
            loading.waitUntil(Condition.hidden, 120000);
        } catch (StaleElementReferenceException | ElementNotFound ex) {
        }
    }

    @Step("Скриншот")
    public void screenshot() {
        Gauge.captureScreenshot();
    }

    @Autowired
    public CommonSteps setScenarioStore(ScenarioStore scenarioStore) {
        this.scenarioStore = scenarioStore;
        return this;
    }

    @Step("Выбрать <1> элемент в селекте <element>")
    public void listSelect(int number, String elementId) {

        scenarioStore.currentContext().set(elementId, number);

    }
}
