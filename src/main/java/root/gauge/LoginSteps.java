package root.gauge;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import com.thoughtworks.gauge.Gauge;
import com.thoughtworks.gauge.Step;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import root.entities.User;
import root.ui.ScenarioStore;
import root.ui.SelenideActions;

import static root.ui.Context.context;

@Component
public class LoginSteps {

    @Autowired
    private ScenarioStore scenarioStore;

    @Step("Залогиниться")
    public void login() {
        User user = scenarioStore.getData(DataKeys.User, User.class);
        SelenideElement element = SelenideActions.selenideElemByElementId("Форма входа");
        element.waitUntil(Condition.visible, 15000);
        context("Форма входа")
                .set("Логин", user.getBrief())
                .set("Пароль", user.getPassword())
                .click("Войти");
        Gauge.writeMessage("Пользователь B2B %s", user.getBrief());

    }
}
