package root.gauge;

import com.codeborne.selenide.Selenide;
import com.thoughtworks.gauge.*;
import org.springframework.stereotype.Component;

@Component
public class Hooks {

    @BeforeSuite
    public void beforeSuite(){

    }

    @BeforeScenario
    public void beforeScenario(){

    }

    @BeforeSpec
    public void beforeSpec(){

    }

    @BeforeStep
    public void beforeStep(){

    }

    @AfterStep
    public void afterStep() {

    }

    @AfterSpec
    public void afterSpec() {
        Selenide.close();
    }

    @AfterScenario
    public void afterScenario(){

    }

    @AfterSuite
    public void afterSuite(){

    }

}
