package root.gauge;

import com.thoughtworks.gauge.Gauge;
import com.thoughtworks.gauge.Step;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import root.db.UserDAO;
import root.db.UserKeeper;
import root.entities.Product;
import root.entities.User;
import root.ui.ScenarioStore;


@Component
public class UserSteps {

    @Autowired
    @Qualifier("employerDAOImpl")
    private UserDAO userDAO;

    @Autowired
    private UserKeeper userKeeper;

    @Autowired
    private ScenarioStore scenarioStore;

    @Step("Найти штатника для продукта <productName> филиал <orgstructureId> ")
    public void findEmpoye(String productName, String orgstructureId) {
        Product product = Product.find(p -> p.getName().equals(productName));
        scenarioStore.putData(DataKeys.Product, product);
        User user = userDAO.getUser(String.valueOf(product.getProductId()), orgstructureId);
        userDAO.updatePassword(user);
        scenarioStore.putData(DataKeys.User, user);
        userKeeper.add(user);
        Gauge.writeMessage("Пользователь найден %s", user.getBrief());
    }
}
