package root.gauge;

import com.thoughtworks.gauge.Step;
import root.ui.Context;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

import static root.ui.Context.context;

public class FooterSteps {
    @Step("Должна быть ошибка <errorMessage>")
    public void checkErrorWithText(String errorMessage) {
        List<String> errorsFromFooter = errorsFromFooter();
        Stream withMessage = errorsFromFooter.stream().filter(line -> line.contains(errorMessage));

        if (!withMessage.iterator().hasNext()) { // нет хотя бы одна ошибки с таким текстом
            throw new AssertionError(String.format("Ошибка с текстом '%s' не найдена среди ошибок валидации: %s", errorMessage, errorsFromFooter));
        }
    }

    @Step("Не должно быть ошибок в Футере")
    public void checkNoErrorsInFooter() {
        context("Футер").checkNotExist("Ошибки");

        List<String> errorsFromFooter = errorsFromFooter();

        if (errorsFromFooter.iterator().hasNext()) { // есть хотя бы одна ошибка
            throw new AssertionError(String.format("Не должно быть ошибок валидации, а имеются: %s", errorsFromFooter));
        }
    }


    public List<String> errorsFromFooter() {
        Context context = context("Футер");

        if (!context.exists("Ошибки")) {
            return Collections.emptyList();
        }

        // прокрутить ошибки на самое начало
        while (!context.disabled("Предыдущее сообщение")) {
            context.click("Предыдущее сообщение");
        }

        // должна быть ошибка - либо текущая, либо надо пролистать
        List<String> errorList = new LinkedList<String>();
        errorList.add(context.value("Ошибка"));

        // если имеется следующее сообщение - если кнопка "Следующее сообщение" разблокирована - тогда на ее кликнуть
        while (!context.disabled("Следующее сообщение")) {
            context.click("Следующее сообщение");
            errorList.add(context.value("Ошибка"));
        }

        return errorList;
    }

}
