package root.gauge;

public class GaugeError extends RuntimeException {

    public GaugeError(String message, Throwable cause) {
        super(message, cause);
    }

    @Override
    public String toString() {
        return getMessage();
    }

}
