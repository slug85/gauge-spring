package root.gauge;

import com.thoughtworks.gauge.screenshot.ICustomScreenshotGrabber;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

@Component
@Lazy
public class SelenideScreenshotGrabber implements ICustomScreenshotGrabber {

      /**
     * Return a screenshot byte array
     */
    public byte[] takeScreenshot() {
        return ((TakesScreenshot) getWebDriver()).getScreenshotAs(OutputType.BYTES);
    }
}
