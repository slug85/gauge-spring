package root.gauge;

import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {

    public static String today() {
        return new SimpleDateFormat("dd.MM.yyyy").format(new Date());
    }

    public static String todayPlusYearDate() {
        return new DateTime().plusYears(1).toString("dd.MM.yyyy");
    }

    public static String todayPlusOneMonth() {
        return new DateTime().plusMonths(1).toString("dd.MM.yyyy");
    }

    public static String todayMinusYear() {
        return String.valueOf(new DateTime().minusYears(1).getYear());
    }

    public static String todayMinusYearDate() {
        return String.valueOf(new DateTime().minusYears(1).toString("dd.MM.yyyy"));
    }

    public static String yesterdayPlusYear() {
        return new DateTime().minusDays(1).plusYears(1).toString("dd.MM.yyyy");
    }

    // СЕГОДНЯ_И_СЕЙЧАС
    public static String todayAndNow() {
        return new DateTime().toString("dd.MM.yyyy HH:mm:ss");
    }

    public static int prevYear(){
        return new DateTime().minusYears(1).getYear();
    }
}
