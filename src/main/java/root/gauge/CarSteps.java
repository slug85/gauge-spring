package root.gauge;

import com.codeborne.selenide.SelenideElement;
import com.thoughtworks.gauge.Step;
import org.openqa.selenium.By;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import root.ui.ScenarioStore;

import static com.codeborne.selenide.Selenide.$;

@Component
public class CarSteps {

    @Autowired
    private ScenarioStore scenarioStore;

    @Autowired
    @Lazy
    private CommonSteps commonSteps;

    @Step("Считать и заполнить стоимость")
    public void setPrice(){
        SelenideElement element = $(By.xpath("//div[contains(text(), \"Стоимость по справочнику\")]"));
        String text = element.getText();
        String[] arr = text.split("—");
        int price = (Integer.parseInt(arr[0].replaceAll("[^0-9]", "")) + Integer.parseInt(arr[1].replaceAll("[^0-9]", "")))/2;
        commonSteps.setElementValue("Стоимость ТС (RUR)", String.valueOf(price));
        scenarioStore.putData(DataKeys.CarPrice, price);
    }
}
