package root.ui;

import com.thoughtworks.gauge.datastore.DataStore;
import com.thoughtworks.gauge.datastore.DataStoreFactory;
import org.springframework.stereotype.Component;
import root.gauge.DataKeys;

import static org.junit.Assert.fail;

@Component
public class ScenarioStore {

    private static String CURRENT_ELEMENT = "CURRENT_ELEMENT";
    static String CURRENT_ELEMENT_ID = "CURRENT_ELEMENT_ID";

    public Context currentContext() {
        return (Context) DataStoreFactory.getScenarioDataStore().get(CURRENT_ELEMENT);
    }

    public void currentContext(Context element) {
        DataStoreFactory.getScenarioDataStore().put(CURRENT_ELEMENT, element);
    }

    public DataStore getDataStore() {
        return DataStoreFactory.getScenarioDataStore();
    }

    @SuppressWarnings("unchecked")
    public <E> E getData(DataKeys dataKeys, Class<E> clazz){
        try {
            return (E)getDataStore().get(dataKeys);
        }catch (ClassCastException ex){
            fail(String.format("объект в хранилище не является объектом класса %s ", clazz.getName()));
            return null;
        }
    }

    public void putData(DataKeys dataKeys, Object obj){
        getDataStore().put(dataKeys, obj);
    }



}
