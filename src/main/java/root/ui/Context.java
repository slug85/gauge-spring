/*************************
 *
 * AIM Consulting
 * ______
 *
 *  [2018] - [2019] AIM Consulting (aimc.io)
 *  All Rights Reserved.
 * NOTICE:  All information contained herein is, and remains
 * the property of AIM Consulting (aimc.io).
 * The intellectual and technical concepts contained
 * herein are proprietary to AIM Consulting and may be covered by
 * Russian, U.S. or other Patents, patents in process, and are
 * protected by trade secret or copyright law.
 * Dissemination of this information or using, copying, distributing,
 * reproduction of this material is strictly forbidden unless prior written
 * permission is obtained from AIM Consulting (aimc.io).
 */

package root.ui;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.ex.UIAssertionError;
import root.gauge.GaugeError;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selectors.withText;
import static root.ui.SelenideActions.getElementType;
import static root.ui.SelenideActions.scrollToCenter;

@SuppressWarnings({"UnusedReturnValue", "JavaDoc", "UnnecessaryLocalVariable"})
public class Context {

    private SelenideElement context;
    private String contextId;

    /**
     * Основной метод работы с библиотекой. Ищет и возвращает контекст, который представляет собой форму, панель,
     * диалог или какую-либо группу элементов. Метод ищет текущий elementId в глобальном контексте.
     *
     * @param elementId Id элемента, который представляет собой контекст. Ищет в глобальном контексте.
     * @return новые объект Context.
     */
    public static Context context(String elementId) {
        try {
            SelenideElement selem = SelenideActions.selenideElemByElementId(elementId);
            return new Context(selem, elementId);
        } catch (UIAssertionError e) {
            throw new SelenideError(String.format("Элемент '%s' не найден в глобальном контексте.\n\nСкриншот: %s\nДетали:%s", elementId, e.getScreenshot(), e.getMessage()), e);
        }
    }


    private SelenideElement childSelenideElem(String elementId) {
        try {
            SelenideElement selem = SelenideActions.childSelenideElem(context, elementId);
            return selem;
        } catch (UIAssertionError e) {
            throw new SelenideError(String.format("Элемент '%s' не найден в контексте '%s'.\n\nСкриншот: %s\n\nДетали:%s", elementId, contextId, e.getScreenshot(), e.getMessage()), e);
        }
    }


    private Context(SelenideElement selenideElement, String elementId) {
        this.context = selenideElement;
        this.contextId = elementId;
    }


    public Context check(String elementId, String text) {
        SelenideElement selem = childSelenideElem(elementId);
        selem.should(exist);

        if ("input".equals(selem.getTagName())) {
            selem.shouldHave(Condition.value(text));
        } else {
            selem.shouldHave(text(text));
        }

        return this;
    }

    /**
     * Проверяет, что на таком элементе нет текста text.
     *
     * @param elementId
     * @param text
     * @return
     */
    public Context checkNoText(String elementId, String text) {
        childSelenideElem(elementId).shouldNotHave(text(text));
        return this;
    }


    public Context waitElem(String elementId) {
        SelenideElement selem = childSelenideElem(elementId);
        selem.waitUntil(visible, Configuration.timeout);
        return this;
    }


    public String value(String elementId) {
        return childSelenideElem(elementId).text();
    }


    public Context set(String elementId, String newValue) {
        SelenideElement selem = childSelenideElem(elementId);
        String tagName = selem.getTagName(); // input, select, button, span
        String tagType = selem.getAttribute("type");

        scrollToCenter(selem);

        if ("select".equals(tagName)) {
            selem.selectOptionContainingText(newValue);
            selem.pressTab();
        } else if ("input".equals(tagName) && "checkbox".equals(tagType)) {
            selem.setSelected(Boolean.getBoolean(newValue)); // TODO: does not work, check it up and remove it
        } else if ("label".equals(tagName) && "checkbox".equals(getElementType(selem))) {
            selem.click();
        } else {
            selem.setValue(newValue);
        }
        selem.pressTab();
        return this;
    }

    public Context setSlowyly(String elementId, String newValue, int delayMillis) {
        SelenideElement selem = childSelenideElem(elementId);

        selem.clear();
        newValue.chars().forEach(c -> {
            selem.sendKeys(Character.toString((char) c));
            try {
                Thread.sleep(delayMillis);
            } catch (InterruptedException ignored) {
            }
        });

        return this;
    }

    public String elementType(String elementId) {
        SelenideCode<String> code = () -> getElementType(childSelenideElem(elementId));
        return handleException(elementId, code);
    }


    @FunctionalInterface
    public interface SelenideCode<R> {
        R apply();
    }

    private <R> R handleException(String elementId, SelenideCode<R> fe) {
        try {
            return fe.apply();
        } catch (UIAssertionError e) {
            throw new SelenideError(String.format("Элемент '%s' не найден в контексте '%s'\n\nСкриншот: %s\n\nДетали:%s", elementId, contextId, e.getScreenshot(), e.getMessage()), e);
        }
    }

    public Context set(String elementId, boolean selected) {
        SelenideCode<Context> code = () -> {

            SelenideElement selem = childSelenideElem(elementId);
            scrollToCenter(selem);
            checkboxValue(elementId, selected);
            return this;

        };
        return handleException(elementId, code);
    }

    public Context set(String elementId, int... index) {
        SelenideCode<Context> code = () -> {

            SelenideElement elem = childSelenideElem(elementId);
            if ("select".equals(elem.getTagName())) {
                elem.selectOption(index);
                elem.pressTab();
            }
            return this;

        };
        return handleException(elementId, code);
    }

    public Context pressEnter(String elementId) {
        SelenideCode<Context> code = () -> {
            childSelenideElem(elementId).pressEnter();
            return this;
        };
        return handleException(elementId, code);
    }

    public Context pressTab(String elementId) {
        SelenideCode<Context> code = () -> {
            childSelenideElem(elementId).pressTab();
            return this;
        };
        return handleException(elementId, code);
    }

    public Context pressEscape(String elementId) {
        SelenideCode<Context> code = () -> {
            childSelenideElem(elementId).pressEscape();
            return this;
        };
        return handleException(elementId, code);
    }


    public Context clickText(String elementId) {
        SelenideCode<Context> code = () -> {
            SelenideElement selem = context.$(withText(elementId));
            scrollToCenter(selem);
            selem.click();
            return this;
        };
        return handleException(elementId, code);
    }


    public Context click(String elementId) {
        SelenideElement selem;

        selem = childSelenideElem(elementId);
        if (selem.is(hidden))
            //если не нашли элемент по data-elem-id, ищем по тексту
            selem = context.$(byText(elementId));

        try {
            scrollToCenter(selem);
            selem.click();
        } catch (UIAssertionError e) {
            throw new SelenideError(String.format("Элемент '%s' не найден в контексте '%s'\n\nСкриншот:" +
                    " %s\n\nДетали:%s", elementId, contextId, e.getScreenshot(), e.getMessage()), e);
        }

        return this;
    }

    public SelenideElement context() {
        return context;
    }

    /**
     * Устанавливает текущее значение в checkbox. Есть особенность - значение true/false хранится в элементе
     * input type="checkbox", а кликать надо на его label.
     *
     * @param elementId
     * @param selected
     * @return
     */
    private Context checkboxValue(String elementId, boolean selected) {
        return handleException(elementId, () -> {
            boolean currentValue = checkboxValue(elementId);
            if (currentValue == selected) {
                return this; // уже то же самое значение стоит
            }

            SelenideElement label = childSelenideElem(elementId);
            label.click();
            label.pressTab();
            return this;
        });
    }

    private boolean checkboxValue(String elementId) {
        SelenideElement label = childSelenideElem(elementId);
        SelenideElement checkbox = childSelenideElem(checkboxId(elementId));

        return Boolean.parseBoolean(checkbox.getAttribute("checked"));
    }

    /**
     * Проверяет, является ли элемент с переданным id checkbox'ом.
     *
     * @param elementId id элемента для проверки.
     * @return true, если элемент является чекбоксом, и false в противном случае.
     */
    public boolean checkbox(String elementId) {
        try {
            SelenideElement selem = childSelenideElem(elementId);
            String tagName = selem.getTagName(); // input, select, button, span
            String tagType = selem.getAttribute("type");
            return "input".equals(tagName) && "checkbox".equals(tagType);
        } catch (Exception e) {
            return false;
        }
    }

    private String checkboxId(String elementId) {
        return elementId + " (Значение)";
    }

    public boolean disabled(String elementId) {
        return handleException(elementId, () -> childSelenideElem(elementId).is(disabled));
    }

    public Context checkNotExist(String elementId) {
        try {
            childSelenideElem(elementId).shouldNot(exist);
            return this;
        } catch (UIAssertionError e) {
            throw new GaugeError(String.format("Не должно быть элемента '%s' в контексте '%s'\n\nСкриншот: %s\n\nДетали: %s", elementId, contextId, e.getScreenshot(), e.getMessage()), e);
        }
    }

    public Context checkNotEmpty(String elementId) {
        try {
            childSelenideElem(elementId).shouldNotBe(empty);
            return this;
        } catch (UIAssertionError e) {
            throw new GaugeError(String.format("Элемент '%s' в контексте '%s' не должен быть пустым\n\nСкриншот: %s\n\nДетали: %s", elementId, contextId, e.getScreenshot(), e.getMessage()), e);
        }
    }

    public Context waitText(String text) {
        try {
            SelenideElement found = context.waitUntil(text(text), Configuration.timeout * 2);
            return this;
        } catch (UIAssertionError e) {
            throw new GaugeError(String.format("Не дождались появления текста '%s' в контексте '%s'\n\nСкриншот: %s\n\nДетали: %s", text, contextId, e.getScreenshot(), e.getMessage()), e);
        }
    }

    public boolean exists(String elementId) {
        return handleException(elementId, () -> childSelenideElem(elementId).exists());
    }

}


