package root.ui;

import com.codeborne.selenide.Configuration;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

@Component
public class SelenideConfig implements InitializingBean {

    @Override
    public void afterPropertiesSet(){
        Configuration.startMaximized = true;
        Configuration.timeout = 10000;
        Configuration.pollingInterval = 500;
        Configuration.fastSetValue = true;
        Configuration.browser = "chrome";
        Configuration.browserCapabilities.setCapability("enableVNC", true);
    }
}
