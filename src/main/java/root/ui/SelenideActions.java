/*************************
 *
 * AIM Consulting
 * ______
 *
 *  [2018] - [2019] AIM Consulting (aimc.io)
 *  All Rights Reserved.

 * NOTICE:  All information contained herein is, and remains
 * the property of AIM Consulting (aimc.io).
 * The intellectual and technical concepts contained
 * herein are proprietary to AIM Consulting and may be covered by
 * Russian, U.S. or other Patents, patents in process, and are
 * protected by trade secret or copyright law.
 * Dissemination of this information or using, copying, distributing,
 * reproduction of this material is strictly forbidden unless prior written
 * permission is obtained from AIM Consulting (aimc.io).
 */

package root.ui;

import com.codeborne.selenide.*;
import org.openqa.selenium.By;

public class SelenideActions {

    private static String ID_ATTRIBUTE_NAME = "data-elem-id";
    private static String TYPE_ATTRIBUTE_NAME = "data-elem-type";

    private static By byElementId(String elementId) {
        return By.cssSelector(String.format("*[" + ID_ATTRIBUTE_NAME + "='%s']", elementId));
    }

    public static SelenideElement selenideElemByElementId(String elementId) {
        ElementsCollection elements = Selenide.$$(byElementId(elementId));
        //ждем появления в DOM всех элементов с указанным id
        elements.shouldHave(CollectionCondition.sizeGreaterThan(0),Configuration.timeout*5);
        //ждем чтобы хоть один из них стал видимым
        elements.filter(Condition.visible).shouldHave(CollectionCondition.sizeGreaterThan(0),Configuration.timeout*5);
        return elements.filter(Condition.visible).first();
    }

    public static SelenideElement childSelenideElem(SelenideElement parent, String elementId) {
        if (parent == null) {
            throw new NullPointerException("Parent element should not be null");
        }

        return parent.$(byElementId(elementId));
    }

    static void scrollToCenter(SelenideElement selem) {
        selem.scrollIntoView("{block: \"center\"}");
    }

    public static String getElementType(SelenideElement selenideElement) {
        return selenideElement.getAttribute(TYPE_ATTRIBUTE_NAME);
    }
}
