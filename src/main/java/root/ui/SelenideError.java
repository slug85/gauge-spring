/*************************
 *
 * AIM Consulting
 * ______
 *
 *  [2018] - [2019] AIM Consulting (aimc.io)
 *  All Rights Reserved.

 * NOTICE:  All information contained herein is, and remains
 * the property of AIM Consulting (aimc.io).
 * The intellectual and technical concepts contained
 * herein are proprietary to AIM Consulting and may be covered by
 * Russian, U.S. or other Patents, patents in process, and are
 * protected by trade secret or copyright law.
 * Dissemination of this information or using, copying, distributing,
 * reproduction of this material is strictly forbidden unless prior written
 * permission is obtained from AIM Consulting (aimc.io).
 */

package root.ui;

public class SelenideError extends RuntimeException {

    public SelenideError(String message, Throwable cause) {
        super(message, cause);
    }

    @Override
    public String toString() {
        return getMessage();
    }

}
