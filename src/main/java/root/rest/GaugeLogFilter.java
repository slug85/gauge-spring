package root.rest;

import com.thoughtworks.gauge.Gauge;
import io.restassured.filter.Filter;
import io.restassured.filter.FilterContext;
import io.restassured.response.Response;
import io.restassured.specification.FilterableRequestSpecification;
import io.restassured.specification.FilterableResponseSpecification;
import org.apache.commons.lang.StringEscapeUtils;

public class GaugeLogFilter implements Filter {

    @Override
    public Response filter(FilterableRequestSpecification requestSpec,
                           FilterableResponseSpecification responseSpec, FilterContext ctx) {
        Response response = ctx.next(requestSpec, responseSpec);
        String requestBuilder = requestSpec.getMethod() +
                "\n" +
                requestSpec.getURI() +
                "\n" +
                requestSpec.getHeaders().asList() +
                "\n" +
                requestSpec.getBody();
        Gauge.writeMessage(StringEscapeUtils.escapeHtml(requestBuilder));
        String responseBuilder = response.getStatusLine() +
                "\n" +
                response.getBody().prettyPrint();
        Gauge.writeMessage(StringEscapeUtils.escapeHtml(responseBuilder));
        return  response;
    }
}
