package root.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.thoughtworks.gauge.Step;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.config.RestAssuredConfig;
import io.restassured.config.SSLConfig;
import io.restassured.internal.matcher.xml.XmlXsdMatcher;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import root.entities.User;
import root.gauge.DataKeys;
import root.setup.FileWorker;
import root.ui.ScenarioStore;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.restassured.config.SSLConfig.sslConfig;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.*;

/**
 * Created by sergey.lugovskoi on 26.02.2019.
 */
@SuppressWarnings("SameParameterValue")
@Component
@Scope("prototype")
public class RESTClient implements InitializingBean {

    private RequestSpecification spec;
    private RequestSpecification graphqlSpec;
    private Map<String, Object> headers = new HashMap<>();
    private Map<String, Object> graphqlHeaders = new HashMap<>();
    private RestAssuredConfig config;

    @Autowired
    private ScenarioStore scenarioStore;

    @Override
    public void afterPropertiesSet(){
        setSSL();
        setGraphqlSpec();
        setGraphqlHeaders();
    }

    @Autowired
    private FileWorker fileWorker;

    @Step("Инициализировать клиент для сервиса <endPointKey>")
    public void setSpecs(String endPointKey) {
        spec = new RequestSpecBuilder()
                .setBaseUri(System.getenv(endPointKey))
                .setConfig(config)
                .addFilter(new GaugeLogFilter())
                .build();
    }

    private void setGraphqlSpec() {
        graphqlSpec = new RequestSpecBuilder()
                .setBaseUri(System.getenv("GRAPHQL"))
                .setConfig(config)
                .addFilter(new GaugeLogFilter())
                .build();
    }

    private void setSSL() {
        SSLConfig sslConfig = sslConfig().allowAllHostnames().relaxedHTTPSValidation();
        config = RestAssured.config().sslConfig(sslConfig);
    }

    @Step("GET запрос путь <urlPart>")
    public Response get(String url){
        Response response = given().log().all().spec(spec).headers(headers).get("/"+url);
        response.then().assertThat().statusCode(200);
        scenarioStore.putData(DataKeys.Response, response);
        return response;
    }

    @Step("Добавить заголовок запроса <key> : <value>")
    public void addHeader(String key, String value) {
        headers.put(key, value);
    }

    @Step("Добавить токен в заголовки запросов")
    public void addTokenToHeaders() {
        headers.put("Authorization", "Bearer " + scenarioStore.getData(DataKeys.User, User.class).getToken());
        graphqlHeaders.put("Authorization", "Bearer " + scenarioStore.getData(DataKeys.User, User.class).getToken());
    }

    @Step("Проверить значение ContentType заголовка ответа <expectedContentType>")
    public void checkResponseContentType(String expectedContentType){
        Response response = scenarioStore.getData(DataKeys.Response, Response.class);
        assertThat(response.statusCode(), is(200));
        String contentType = response.contentType();
        if(!contentType.contains(expectedContentType)){
            fail("Запрос вернул неправильный ContentType " + contentType);
        }
    }

    @Step("Проверить ответ на соответствие xmlSchema <xmlSchemaFilePath>")
    public void checkResponseXMLSchema(String xmlSchemaFilePath) {
        Response response = scenarioStore.getData(DataKeys.Response, Response.class);
        assertThat(response.getBody().asString(), XmlXsdMatcher.matchesXsdInClasspath(xmlSchemaFilePath));
    }

    @Step("Проверить значение поля в JSON через JsonPath <path> <value> ")
    public void assertJsonAttributeWithPath(String path, String value) {
        Response response = scenarioStore.getData(DataKeys.Response, Response.class);
        JsonPath jsonPath = response.jsonPath();
        String resValue = jsonPath.getString(path);
        Assert.assertEquals(resValue, value);
    }

    @Step("Проверить значение поля в XML <attribute> <value>")
    public void assertXmlAttribute(String attribute, String value) {
        Response response = scenarioStore.getData(DataKeys.Response, Response.class);
        response.then().assertThat().body("**.find{ it.@name == '" + attribute + "' }.@value", equalTo(value));
    }

    @Step("Проверить ответ на соответствие jsonSchema <jsonSchemaFilePath>")
    public void checkResponseJsonSchema(String jsonSchemaFilePath){
        Response response = scenarioStore.getData(DataKeys.Response, Response.class);
        assertThat(response.getBody().asString(), matchesJsonSchemaInClasspath(jsonSchemaFilePath));
    }

    @Step("POST запрос json из файла <fileName>, путь <urlPart>")
    public Response postJson(String fileName, String urlPart) {
        String jsonBody = fileWorker.read(fileName);
        Response response = postJsonString(urlPart, jsonBody);
        return response;
    }

    private Response postJsonString(String urlPart, String jsonBody) {
        Response response = given().log().all().spec(spec).headers(headers).body(jsonBody).post("/"+urlPart);
        scenarioStore.putData(DataKeys.Response, response);
        response.then().assertThat().statusCode(200);
        return response;
    }

    private void setGraphqlHeaders() {
        graphqlHeaders = new HashMap<>();
        graphqlHeaders.put("Content-Type", "application/json" );
    }

    @Step("Добавить заголовок запроса Content-Type : <value>")
    public void addContentTypeHeader(String value) {
        addHeader("Content-Type", value);
    }

    private Response postGraphql(String query, String variables){

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode req = mapper.createObjectNode();
        req.put("query", query);

        if(variables != null && !variables.isEmpty()){
            req.put("variables", variables);
        }

        Response response = null;
        try {
            response = given().log().all().spec(graphqlSpec).headers(graphqlHeaders).body(mapper.writeValueAsString(req)).post();
        } catch (JsonProcessingException ex) {
            ex.printStackTrace();
        }
        scenarioStore.putData(DataKeys.Response, response);
        return response;

    }

    /**
     *
     * @param filePath путь к файлу запроса к graphql
     * @param variables переменные значения в запросе graphql должны быть заданы в виде %s, дальше в коде
     *                  на их место буду подставлены  значения из variables в порядке появления
     * @return ответ. также соханяется в scvenarioStore
     */
    private Response postGraphqlFromFile(String filePath, String... variables){

        String query = fileWorker.read(filePath);

        if(variables.length > 0){
            for (String variable : variables) {
                int index = query.indexOf("%s");
                int length = query.length();
                query = query.substring(0, index) + variable + query.substring(index + 2, length);
            }
        }


        Response response;
        response = given().log().all().spec(graphqlSpec).headers(graphqlHeaders).body(query).post();
        scenarioStore.putData(DataKeys.Response, response);
        assertThat(response.statusCode(), is(200));
        return response;

    }

    @Step("GraphQl создать страхователя")
    public void insurerUpdate(){
        //добавить страхователя
        Response response =  postGraphqlFromFile("data/services/graphql/update_person.json");
        Integer objectId = response.getBody().jsonPath().get("data.updateObject.IDObekta");
        scenarioStore.putData(DataKeys.InsurerId, objectId);

        //получить данные по добаленному КА

        response = postGraphqlFromFile("data/services/graphql/get_person.json", String.valueOf(objectId));
        scenarioStore.putData(DataKeys.AddressId, response.getBody().jsonPath().get("data.entityObject.Adresa[0].IDObekta"));
        scenarioStore.putData(DataKeys.PassportId, response.getBody().jsonPath().get("data.entityObject.Udostovereniya[0].IDObekta"));
        scenarioStore.putData(DataKeys.VUId, response.getBody().jsonPath().get("data.entityObject.Udostovereniya[1].IDObekta"));
        scenarioStore.putData(DataKeys.Mobile, response.getBody().jsonPath().get("data.entityObject.Kontakty[0].IDObekta"));


    }

    @Step({"Получить токен graphql логин <userName> пароль <password>"})
    public String auth(String userName, String password){
        String query = Queries.auth;
        String variables = String.format("{\"username\":\"%s\",\"password\":\"%s\"}", userName, password);
        Response res = postGraphql(query, variables);
        JsonPath jsonPath = res.jsonPath();
        String error = jsonPath.get("data.auth.diasoft.login.error");
        String token = jsonPath.get("data.auth.diasoft.login.access_token");
        assertNull("найдена ошибка в ответе graphql", error);
        assertNotNull("не удалось получить токен", token);
        User user = new User();
        user.setBrief(userName);
        user.setPassword(password);
        user.setToken(token);
        scenarioStore.putData(DataKeys.User, user);
        return token;

    }

    @Step("Сохранить значение из JSON <key>")
    public void saveValueFromJson (String key) {
        String value;
        Response response = scenarioStore.getData(DataKeys.Response, Response.class);
        value = response.jsonPath().getString(key);
        scenarioStore.putData(DataKeys.TempValue, value);
    }

    @Step("Проверка созданной заявки СБ")
    public void eventCheckSb() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonObject = mapper.createObjectNode();
        jsonObject.put("objectId", scenarioStore.getData(DataKeys.TempValue, String.class));
        jsonObject.put("comment", "Тестовый комментарий");
        String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonObject);
        postJsonString("eventchecksb", json);
        //checkResponseJsonSchema("data/services/eventCheckSb.json");
    }

    @Step("Проверка полей заявки")
    public void fields() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonObject = mapper.createObjectNode();
        jsonObject.put("objectId", scenarioStore.getData(DataKeys.TempValue, String.class));
        String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonObject);
        postJsonString("fields", json);
        //checkResponseJsonSchema("data/json-schemas/fieldsSchema.json");
    }

}
