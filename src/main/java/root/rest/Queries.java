package root.rest;

/**
 * Created by sergey.lugovskoi on 21.08.2018.
 */
class Queries {

     final static String auth =
            " query login($username: String!, $password: String!)" +
            "{ " +
            "  auth {   " +
            "    diasoft {   " +
            "    login(username: $username, password: $password) {" +
            "      error" +
            "      errorcode" +
            "      access_token" +
            "    }" +
            "    }" +
            "  }" +
            "}";
}